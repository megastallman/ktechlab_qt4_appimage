# ktechlab_qt4_appimage

This is a qt4-based Ktechlab Appimage builder. See more at: https://github.com/ktechlab

1) cd ubuntu-14.04 
2) ./ubuntu1404-ktechlab-builder.sh
3) ./ubuntu1404-ktechlab-linuxdeployqt-builder.sh

You can partially reuse this project to build appimages of any other QT4-based application. To revive any QT3 applications, see: https://gitlab.com/megastallman/ktechlab-0.3.7-appimage
