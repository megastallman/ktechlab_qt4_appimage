#sudo apt update
#sudo apt -y install vim git libqt4-dev cmake gpsim-dev kdelibs5-dev libglib2.0-dev gpsim build-essential qt4-dev-tools
rm -rf ktechlab || true
rm -rf appdir || true
rm -rf /tmp/ktechlab-prefix || true
git clone https://github.com/ktechlab/ktechlab.git
cd ktechlab
mkdir build
cd build/
#cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH="/tmp/ktechlab-prefix/usr" -Wno-dev ..
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -Wno-dev ..
#cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH="/usr" -Wno-dev ..
make -C src/core
make
make DESTDIR=appdir install ; find . -name appdir
#make INSTALL_ROOT="/tmp/ktechlab-prefix/usr" install
