#!/bin/bash

DL_LINK="https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
rm -rf ktechlab4.AppDir || true
rm KTechlab-x86_64.AppImage || true
mkdir ktechlab4.AppDir
cp ktechlab.png ktechlab4.AppDir

cat > ktechlab4.AppDir/AppRun << EOF
#!/bin/sh
CUR_DIR=\$(dirname "\$(readlink -f "\$0")")
export PATH="\${CUR_DIR}/usr/bin:\$PATH"
export LD_LIBRARY_PATH="\${LD_LIBRARY_PATH}:\${CUR_DIR}/usr/lib"
echo "Current directory is: \${CUR_DIR}"
mkdir /tmp/ktechlab || /bin/true
ln -fsT $PAR/usr /tmp/ktechlab/usr || /bin/true
ktechlab
EOF
chmod +x ktechlab4.AppDir/AppRun

cat > ktechlab4.AppDir/ktechlab.desktop << EOF
[Desktop Entry]
Name=KTechlab
Name[ca]=KTechlab
Name[ca@valencia]=KTechlab
Name[cs]=KTechlab
Name[de]=KTechlab
Name[es]=KTechlab
Name[fr]=KTechlab
Name[it]=KTechlab
Name[nl]=KTechlab
Name[pt]=KTechlab
Name[pt_BR]=KTechlab
Name[sk]=KTechlab
Name[sv]=Ktechlab
Name[uk]=KTechlab
Name[x-test]=xxKTechlabxx
Exec=ktechlab %i -caption %c
Icon=ktechlab
Type=Application
X-DocPath=ktechlab/index.html
Comment=An IDE for microcontrollers and electronics
Comment[ca]=Un IDE per a microcontroladors i electrònica
Comment[ca@valencia]=Un IDE per a microcontroladors i electrònica
Comment[de]=Eine Entwicklungsumgebung für Mikrocontroller und Elektronik
Comment[es]=Un IDE para microcontroladores y sistemas electrónicos
Comment[fr]=Un Environnement de Développement Intégré pour les microcontrôleurs et l'électronique
Comment[it]=Un IDE per microcontrollori ed elettronica
Comment[nl]=Een IDE (ontwikkelomgeving) voor microcontrollers en elektronica
Comment[pt]=Um IDE para micro-controladores e electrónica
Comment[pt_BR]=Uma IDE para microcontroladores e eletrônicos
Comment[sk]=IDE pre mikrokontroléry a elektroniku
Comment[sv]=En integrerad utvecklingsmiljö för mikrokontroller och elektronik
Comment[uk]=Комплексне середовище розробки для мікроконтролерів та електроніки
Comment[x-test]=xxAn IDE for microcontrollers and electronicsxx
Terminal=false
MimeType=application/x-ktechlab;application/x-flowcode;application/x-circuit;application/x-microbe;
Categories=Qt;KDE;Education;Science;Electronics;
EOF
chmod +x ktechlab4.AppDir/ktechlab.desktop

wget -c ${DL_LINK}
chmod +x *.AppImage

rsync -a /tmp/ktechlab/usr ktechlab4.AppDir --exclude="src"

./appimagetool-x86_64.AppImage ktechlab4.AppDir

rm -rf ktechlab4.AppDir || true
