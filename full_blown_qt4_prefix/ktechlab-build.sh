#!/bin/bash

#sudo apt install -yy git wget cmake build-essential libbz2-dev libacl1-dev libsm-dev flex bison vim docbook-xml docbook-xsl python-dev libdbus-1-dev libicu-dev libx11-dev libpulse-dev libxext-dev libxinerama-dev libxv-dev libxi-dev libxrender-dev libxrandr-dev libxcursor-dev libxft-dev libgles1-mesa-dev libgles2-mesa-dev libgtk2.0-dev
#sudo apt install -yy git wget cmake automoc build-essential libbz2-dev libacl1-dev libsm-dev flex bison vim docbook-xml docbook-xsl python-dev

TARBALLS="http://archive.ubuntu.com/ubuntu/pool/universe/q/qt4-x11/qt4-x11_4.8.7+dfsg.orig.tar.xz
http://archive.ubuntu.com/ubuntu/pool/universe/p/phonon/phonon_4.10.1.orig.tar.xz
http://archive.ubuntu.com/ubuntu/pool/universe/libd/libdbusmenu-qt/libdbusmenu-qt_0.9.3+16.04.20160218.orig.tar.gz
http://archive.ubuntu.com/ubuntu/pool/universe/k/kde4libs/kde4libs_4.14.38.orig.tar.xz
http://downloads.sourceforge.net/libmng/libmng-2.0.2.tar.xz
http://zlib.net/zlib-1.2.11.tar.xz
http://www.ijg.org/files/jpegsrc.v8d.tar.gz
https://downloads.sourceforge.net/giflib/giflib-5.1.4.tar.bz2
http://download.kde.org/stable/attica/attica-0.4.2.tar.bz2
http://openssl.cs.utah.edu/source/old/1.0.1/openssl-1.0.1f.tar.gz
https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.bz2
https://downloads.sourceforge.net/libpng/libpng-1.6.35.tar.xz
http://xmlsoft.org/sources/libxml2-2.9.9.tar.gz
http://xmlsoft.org/sources/libxslt-1.1.33.tar.gz
http://ftp.gnome.org/pub/gnome/sources/glib/2.56/glib-2.56.1.tar.xz
https://sourceware.org/ftp/libffi/libffi-3.2.1.tar.gz
http://archive.ubuntu.com/ubuntu/pool/main/a/automoc/automoc_1.0~version-0.9.88.orig.tar.gz
"
export SLEEP_TIMER=0
export PREFIX="/tmp/ktechlab/usr"
export QT_QT_INCLUDE_DIR="$PREFIX/include"
export C_INCLUDE_PATH="$PREFIX/include"
export CPLUS_INCLUDE_PATH="$PREFIX/include"
export CPPFLAGS="-I$PREFIX/include"
export LDFLAGS="-L$PREFIX/lib"
export PKG_CONFIG_PATH="$PREFIX/lib/pkgconfig"
export PROJDIR=$PWD
export LANG="C"
export PATH="$PREFIX/bin:$PATH"
export SRC="$PREFIX/src"
export MANPATH="$PREFIX/man"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$PREFIX/lib"
export CPATH=$PREFIX/include:$CPATH

mkdir -p "$PREFIX" || true
mkdir "${PREFIX}/lib" || true

if [ ! -d "$SRC" ]; then
	mkdir $SRC || true
        cd $SRC
        git clone git://anongit.kde.org/ktechlab.git --depth 1 $SRC/ktechlab || true
        #git clone https://github.com/ktechlab/ktechlab.git --depth 1 $SRC/ktechlab || true
	for TARBALL in $TARBALLS
		do
			wget --retry-connrefused --waitretry=5 -c $TARBALL
		done
		find . -name '*.tar.*' -exec tar -xf {} \;
	fi


#if !;then

echo "UGLY HACKS ====================================="
cd $PREFIX/lib
#cp x86_64-linux-gnu/lib* .
ln -sf . x86_64-linux-gnu

echo "BUILDING ZLIB ============================================"
cd $SRC/zlib-1.2.11
./configure --prefix=$PREFIX --shared
make
make install
sleep $SLEEP_TIMER

#echo "BUILDING LIBPNG =========================================="
#cd $SRC/libpng-1.6.35/
#patch -p1 < $PROJDIR/libpng-1.6.35-apng.patch
#LIBS=-lpthread ./configure --prefix=$PREFIX --disable-static
#make
#make install
#sleep $SLEEP_TIMER

echo "BUILDING LIBFFI =========================================="
cd $SRC/libffi-3.2.1
sed -e '/^includesdir/ s/$(libdir).*$/$(includedir)/' -i include/Makefile.in
sed -e '/^includedir/ s/=.*$/=@includedir@/'  -e 's/^Cflags: -I${includedir}/Cflags:/' -i libffi.pc.in
./configure --prefix=$PREFIX --disable-static
make
make install
sleep $SLEEP_TIMER

echo "BUILDING PCRE ============================================"
cd $SRC/pcre-8.43
./configure --prefix=$PREFIX --docdir=$PREFIX/share/doc/pcre-8.43 --enable-unicode-properties --enable-pcre16 --enable-pcre32 --enable-pcregrep-libz --disable-static 
make
make install
cd "${PREFIX}/lib"
ln -sT libpcre.so libpcre.so.3
sleep $SLEEP_TIMER

echo "BUILDING OPENSSL ========================================="
cd $SRC/openssl-1.0.1f
./config --prefix=$PREFIX --openssldir="${PREFIX}/etc/ssl" --libdir=lib shared zlib-dynamic
make
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
#make MANSUFFIX=ssl install
make install_sw
sleep $SLEEP_TIMER

echo "BUILDING LIBPNG =========================================="
cd $SRC/libpng-1.6.34
./configure --prefix=$PREFIX
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBGIF =========================================="
cd $SRC/giflib-5.1.4
sed -i "s:#include <jpeg:#include <stdio.h>\n&:" libmng_types.h
./configure --prefix=$PREFIX --disable-static
make
make install
sleep $SLEEP_TIMER

echo "BUILDING JPEG ============================================"
cd $SRC/jpeg-8d
./configure --prefix=$PREFIX --enable-shared
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBMNG =========================================="
cd $SRC/libmng-2.0.2
sed -i "s:#include <jpeg:#include <stdio.h>\n&:" libmng_types.h
./configure --prefix=$PREFIX --disable-static
make 
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBXML2 ========================================="
cd $SRC/libxml2-2.9.9
./configure --prefix=$PREFIX --disable-static
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBXSLT ========================================="
cd $SRC/libxslt-1.1.33
sed -i s/3000/5000/ libxslt/transform.c doc/xsltproc.{1,xml}
./configure --prefix=$PREFIX --disable-static
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBGLIB2 ========================================"
cd $SRC/glib-2.56.1
./configure --prefix=$PREFIX --enable-libmount=no
make
make install
sleep $SLEEP_TIMER

echo "BUILDING QT4 ============================================="
# https://doc.qt.io/archives/qt-4.8/configure-options.html
cd $SRC/qt-everywhere-opensource-src-4.8.7
patch -p1 < $PROJDIR/qt4-from-archlinux/improve-cups-support.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/moc-boost-workaround.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/kubuntu_14_systemtrayicon.diff
patch -p0 < $PROJDIR/qt4-from-archlinux/kde4-settings.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/glib-honor-ExcludeSocketNotifiers-flag.diff
patch -p1 < $PROJDIR/qt4-from-archlinux/disable-sslv3.patch
#echo "7"
#patch -p1 < $PROJDIR/qt4-from-archlinux/l-qclipboard_fix_recursive.patch
#echo "8"
#patch -p1 < $PROJDIR/qt4-from-archlinux/l-qclipboard_delay.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/qt4-gcc6.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/qt4-glibc-2.25.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/qt4-icu59.patch
patch -p1 < $PROJDIR/qt4-from-archlinux/qt4-openssl-1.1.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-trait-fix.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-ugly-workaround1.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-ugly-workaround2.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-int-cast1.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-int-cast2.patch
patch -p1 < $PROJDIR/qt4-patches/qt-everywhere-opensource-src-4.8.6-null1.patch
sed -i 's/read commercial/commercial=o/' configure
sed -i 's/read acceptance/acceptance=yes/' configure
./configure -confirm-license -prefix "$PREFIX" -opensource -fast -no-multimedia -svg -no-phonon -no-phonon-backend -release -optimized-qmake -dbus-linked -reduce-relocations -no-separate-debug-info -verbose -gtkstyle -no-openvg -qvfb -icu -no-sql-ibase -no-sql-oci -no-sql-odbc -no-sql-sqlite -no-sql-sqlite2 -no-sql-mysql -no-sql-psql -no-sql-db2 -qt-libmng -qt-libjpeg -qt-libtiff -qt-libpng -qt-zlib -no-exceptions -no-webkit -graphicssystem raster -openssl-linked -nomake demos -nomake examples -nomake docs 
make
make install
sleep $SLEEP_TIMER

echo "BUILDING AUTOMOC ========================================="
cd $SRC/automoc-1.0~version-0.9.88
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_BUILD_TYPE=Release ..
make
make install
sleep $SLEEP_TIMER
exit

echo "BUILDING PHONON =========================================="
cd $SRC/phonon-4.10.1
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_BUILD_TYPE=Release -DWITH_DOC=OFF -Wno-dev ..
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBDBUSMENU-QT ==================================="
cd $SRC/libdbusmenu-qt-0.9.3+16.04.20160218
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DUSE_QT4=true -DCMAKE_BUILD_TYPE=Release -DWITH_DOC=OFF -Wno-dev ..
make
make install
sleep $SLEEP_TIMER

echo "BUILDING LIBATTICA ========================================"
cd $SRC/attica-0.4.2
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DQT4_BUILD=ON -DCMAKE_BUILD_TYPE=Release -DWITH_DOC=OFF -Wno-dev ..
make
make install
sleep $SLEEP_TIMER

echo "BUILDING KDELIBS =========================================="
cd $SRC/kdelibs-4.14.38
which qmake
patch -p1 < $PROJDIR/kdelibs-patches/disable_kde_status_notifier.patch
patch -p1 < $PROJDIR/kdelibs-patches/Skip-qtwebkit-parts.patch
patch -p1 < $PROJDIR/kdelibs-patches/add_debian_build_type.diff
patch -p1 < $PROJDIR/kdelibs-patches/add_dlrestrictions_support.diff
patch -p1 < $PROJDIR/kdelibs-patches/add_missing_cast.diff
patch -p1 < $PROJDIR/kdelibs-patches/cmake_compatibility.diff
patch -p1 < $PROJDIR/kdelibs-patches/debian_menu.diff
patch -p1 < $PROJDIR/kdelibs-patches/debian_standardsdirtest.diff
patch -p1 < $PROJDIR/kdelibs-patches/default_kde4_xdg_menu_prefix.diff
patch -p1 < $PROJDIR/kdelibs-patches/disable_bogus_tests.diff
patch -p1 < $PROJDIR/kdelibs-patches/disable_usr_lib_install_rpath.diff
patch -p1 < $PROJDIR/kdelibs-patches/findqt4_optional_x11_pthread.diff
patch -p1 < $PROJDIR/kdelibs-patches/findservicebydesktoppath_try_realfilepath.diff
patch -p1 < $PROJDIR/kdelibs-patches/fix_solidlex_destroy_signature.patch
patch -p1 < $PROJDIR/kdelibs-patches/glibc_filesystem.diff
patch -p1 < $PROJDIR/kdelibs-patches/hack_in_etc_kde4_in_kstandarddirs.diff
patch -p1 < $PROJDIR/kdelibs-patches/hardcode_ptm_device.diff
patch -p1 < $PROJDIR/kdelibs-patches/kconf_update_migrate_from_kde3_icon_theme.diff
patch -p1 < $PROJDIR/kdelibs-patches/kde4libs_interpret_qvarianttype_as_qmetatypetype.diff
#####patch -p1 < $PROJDIR/kdelibs-patches/kdelibs-4.14.38-openssl-1.1.patch
patch -p1 < $PROJDIR/kdelibs-patches/kfileshare_kdesu_fileshareset.diff
patch -p1 < $PROJDIR/kdelibs-patches/ktar_header_checksum_fix.diff
patch -p1 < $PROJDIR/kdelibs-patches/ktar_longlink_length_in_bytes.diff
patch -p1 < $PROJDIR/kdelibs-patches/kubuntu_raise_after_drkonqi.patch
patch -p1 < $PROJDIR/kdelibs-patches/kubuntu_revert_findpythonlibrary.diff
patch -p1 < $PROJDIR/kdelibs-patches/ld_exclude_libs_qtuitools.diff
patch -p1 < $PROJDIR/kdelibs-patches/make_libkdeinit4_private.diff
patch -p1 < $PROJDIR/kdelibs-patches/qt4_designer_plugins_path.diff
patch -p1 < $PROJDIR/kdelibs-patches/relax_plugin_kde_version_check.diff
patch -p1 < $PROJDIR/kdelibs-patches/set_cmake_policies.patch
patch -p1 < $PROJDIR/kdelibs-patches/solid_use_shortest_filepath.diff
patch -p1 < $PROJDIR/kdelibs-patches/test_image-x-compressed-xcf.diff
patch -p1 < $PROJDIR/kdelibs-patches/use_an_oxygen5_file.diff
patch -p1 < $PROJDIR/kdelibs-patches/use_dejavu_as_default_font.diff
patch -p1 < $PROJDIR/kdelibs-patches/remove_udisks2_from_solid_cmaketxt.patch
patch -p1 < $PROJDIR/kdelibs-patches/remove_udisks2_from_solid.patch
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH="$PREFIX" -DBIN_INSTALL_DIR:PATH="$PREFIX/bin" -DLIB_INSTALL_DIR:PATH="$PREFIX/lib" -DINCLUDE_INSTALL_DIR:PATH="$PREFIX/include" -DDBUSMENUQT_INCLUDE_DIR="$PREFIX/include/dbusmenu-qt" -DDBUSMENUQT_LIBRARIES="$PREFIX/lib" -DCMAKE_DISABLE_FIND_PACKAGE_ACL=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_OpenEXR=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Avahi=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_FAM=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_HUpnp=TRUE -Wno-dev ..
make
sed -i "s#/usr/lib/x86_64-linux-gnu/qt4#$PREFIX#g" install_manifest.txt
sed -i "s#/usr/lib/x86_64-linux-gnu/qt4#$PREFIX#g" kdewidgets/cmake_install.cmake
make install
sleep $SLEEP_TIMER


echo "BUILDING KTECHLAB =========================================="
cd $SRC/ktechlab
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH="$PREFIX" -DBIN_INSTALL_DIR:PATH="$PREFIX/bin" -DLIB_INSTALL_DIR:PATH="$PREFIX/lib" -DINCLUDE_INSTALL_DIR:PATH="$PREFIX/include" -Wno-dev ..
make -C src/core
make
make install

